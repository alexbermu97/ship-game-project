using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipSpawnPlayer : MonoBehaviour
{
    [SerializeField]
    private GameObject player;
    [SerializeField]
    private ShipMovement shipMovement;
    [SerializeField]
    private Transform playerSpawnPos;
    [SerializeField]
    private GameObject shipCanvas;
    [SerializeField]
    private CustomizationManager playerAspect;

    void Start()
    {
        playerAspect = player.GetComponentInChildren<CustomizationManager>();
        playerSpawnPos = GameObject.Find("PlayerSpawnPos").transform;
        player.SetActive(false);
        playerAspect.LoadAspect();
    }

    private void Update()
    {
        player.transform.eulerAngles = new Vector3(0, playerSpawnPos.rotation.y, 0);
    }

    public void SpawnPlayer()
    {
        shipCanvas.SetActive(false);
        shipMovement.enabled = false;
        player.SetActive(true);
        player.GetComponent<PlayerMovement>().onGround = false;
    }
}