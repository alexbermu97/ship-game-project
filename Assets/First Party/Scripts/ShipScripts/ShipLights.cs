using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipLights : MonoBehaviour
{
    public bool lightsOn;
    public GameObject[] lights;

    void Update()
    {
        
    }

    public void TurnLights()
    {
        int cont = 0;
        if (!lightsOn && cont == 0)
        {
            lightsOn = true;
            cont++;
        }
        else if (lightsOn && cont == 0)
        {
            lightsOn = false;
            cont++;
        }

        if (lightsOn)
        {
            for (int i = 0; i < lights.Length; i++)
            {
                lights[i].gameObject.SetActive(true);
            }
        }
        else
        {
            for (int i = 0; i < lights.Length; i++)
            {
                lights[i].gameObject.SetActive(false);
            }
        }
    }
}