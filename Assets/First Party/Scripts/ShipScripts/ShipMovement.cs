using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShipMovement : MonoBehaviour
{
    Rigidbody rb;
    ShipThrottle throt;
    public float shipCurrentSpeed;
    public float shipBoostSpeed;
    [Range(0, 100)]
    public float shipMaxSpeed;
    [Range(0, 100)]
    public float shipLeftRightMaxSpeed;
    [Range(0, 100)]
    public float shipUpDownMaxSpeed;

    [Range(0, 1)]
    public float vRotSpeed;
    public float currentVRotSpeed;
    [Range(0, 1)]
    public float hRotSpeed;
    public float currentHRotSpeed;

    [SerializeField]
    private VariableJoystick movementJ;
    [SerializeField]
    private VariableJoystick rotationJ;

    //Gyroscope
    private Gyroscope gyro;
    [SerializeField]
    private bool gyroEnabled;
    private float gyroRot;
    [Range(0, 1)]
    public float rollRotSpeed;
    public float currentRollRotSpeed;

    public Text gyrodata;

    void Start()
    {
        throt = GetComponent<ShipThrottle>();
        rb = GetComponent<Rigidbody>();
        gyroEnabled = EnableGyro();
    }

    void Update()
    {
        //GyroscopeRotation();
    }

    void FixedUpdate()
    {
        Movement();
        Rotation();
        //GyroscopeRotation();
    }

    /*void Speed()
    {
        shipCurrentSpeed = shipMaxSpeed * throt.throttle * 10;
        rb.velocity = new Vector3(rb.velocity.x, rb.velocity.y, shipCurrentSpeed) * Time.deltaTime;
    }*/
    void Movement()
    {
        //Velocity
        float h = movementJ.Horizontal;
        float v = movementJ.Vertical;
        shipCurrentSpeed = shipMaxSpeed * throt.throttle;
        /*if (!throt.turbo)
            shipCurrentSpeed = shipMaxSpeed * throt.throttle;
        else
            shipCurrentSpeed = shipBoostSpeed;
        rb.velocity = new Vector3(shipLeftRightMaxSpeed * h, shipUpDownMaxSpeed * v, shipCurrentSpeed) * 10 * Time.deltaTime;*/

        //Forces
        if (throt.turbo && throt.throttle >= 0.1f)
            rb.AddRelativeForce(Vector3.forward * shipBoostSpeed * 4);
        else if (throt.throttle >= 0.1f || throt.throttle <= -0.1f)
            rb.AddRelativeForce(Vector3.forward * shipCurrentSpeed);
        if (h != 0)
            rb.AddRelativeForce(Vector3.right * h * shipLeftRightMaxSpeed);
        if (v != 0)
            rb.AddRelativeForce(Vector3.up * v * shipUpDownMaxSpeed);
    }
    void Rotation()
    {
        float h = rotationJ.Horizontal;
        float v = rotationJ.Vertical;
        currentHRotSpeed = h * hRotSpeed * 2;
        currentVRotSpeed = -v * vRotSpeed * 2;

        //Forces
        if (h != 0)
            rb.AddRelativeTorque(Vector3.up * currentHRotSpeed);
        if (v != 0)
            rb.AddRelativeTorque(Vector3.right * currentVRotSpeed);
    }

    #region Gyroscope
    private bool EnableGyro()
    {
        if (SystemInfo.supportsGyroscope)
        {
            gyro = Input.gyro;
            gyro.enabled = true;
            return true;
        }
        return false;
    }

    void GyroscopeRotation()
    {
        if (gyroEnabled)
        {
            float r = gyro.rotationRateUnbiased.z * rollRotSpeed;
            gyroRot += r;
            //currentRollRotSpeed = r * rollRotSpeed * 2;
            Vector3 newRot = new Vector3(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, gyroRot);
            if (r >= 0.2f || r <= -0.2f)
                transform.eulerAngles = newRot;

            /*if (r >= 0.2f || r <= -0.2f)
                rb.AddRelativeTorque((Vector3.forward + newRot) * currentRollRotSpeed);*/
            gyrodata.text = "gyro attitude: " + gyro.attitude +
                "\n" + 
                "gyro rotation rate x: " + gyro.rotationRateUnbiased.x + 
                "\n" +
                "gyro rotation rate y: " + gyro.rotationRateUnbiased.y +
                "\n" +
                "gyro rotation rate z: " + gyro.rotationRateUnbiased.z +
                "\n" +
                "r: " + r + 
                "\n" + 
                "roll speed: " + currentRollRotSpeed + 
                "vector new rotation: " + newRot;
        }
    }
    #endregion
}