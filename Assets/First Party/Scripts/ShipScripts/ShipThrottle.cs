using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShipThrottle : MonoBehaviour
{
    [SerializeField]
    private ParticleSystem parts;
    [SerializeField]
    private Slider slider;

    [Range(0, 1)]
    public float throttle;

    public bool turbo;

    void Update()
    {
        Throttle();
    }

    void Throttle() 
    {
        throttle = slider.value;
        if (throttle >= 0.1f)
        {
            if (!turbo)
            {
                parts.startSpeed = throttle * 0.75f;
                parts.emissionRate = 450;
            }
            else
            {
                parts.startSpeed = 2;
                parts.emissionRate = 650;
            }
        }
        else
            parts.startSpeed = 0;
    }

    public void Boost()
    {
        int cont = 0;
        if (!turbo && cont == 0)
        {
            turbo = true;
            cont++;
        }
        if (turbo && cont == 0)
        {
            turbo = false;
            cont++;
        }
    }
}