using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveSystem
{
    //Data is saved in .wfs (stands for whispfire studio)

    //Save and load the player's look
    public static void SavePlayerAspect(CustomizationManager playerAspect)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/playeraspect.wfs";
        FileStream stream = new FileStream(path, FileMode.Create);

        PlayerAspectData data = new PlayerAspectData(playerAspect);

        formatter.Serialize(stream, data);
        stream.Close();
    }
    public static PlayerAspectData LoadPlayerAspect()
    {
        string path = Application.persistentDataPath + "/playeraspect.wfs";
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            PlayerAspectData data = formatter.Deserialize(stream) as PlayerAspectData;
            stream.Close();

            return data;
        }
        else
        {
            Debug.LogError("Save file not found in " + path);
            return null;
        }
    }
}