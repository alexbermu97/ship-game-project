using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerAspectData
{
    //Character Aspect
    public int hairModel;
    public int hairColor;
    public int skinColor;

    public PlayerAspectData (CustomizationManager customManager)
    {
        hairModel = customManager.hairIndex;
        hairColor = customManager.hairColorIndex;
        skinColor = customManager.skinColorIndex;
    }
}