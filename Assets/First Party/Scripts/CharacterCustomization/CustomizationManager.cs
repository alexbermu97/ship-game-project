using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CustomizationManager : MonoBehaviour
{
    enum AppearanceDetail
    {
        Hair_Model,
        Hair_Color,
        Skin_Color,
        Clothes
    }

    //Hair Model
    [SerializeField]
    private GameObject[] hairModels;
    [SerializeField]
    private Transform hairAnchor;

    GameObject activeHair;

    [HideInInspector]
    public int hairIndex = 0;

    //Hair Color
    [SerializeField]
    private Material[] hairMats;

    [HideInInspector]
    public int hairColorIndex = 0;

    //Skin Color
    [SerializeField]
    private Material[] skinMats;
    [SerializeField]
    private MeshRenderer[] bodyParts;

    [HideInInspector]
    public int skinColorIndex = 0;

    //Clothes

    private void Start()
    {
        ApplyMod(AppearanceDetail.Hair_Model, hairIndex);
        ApplyMod(AppearanceDetail.Hair_Color, hairColorIndex);
        ApplyMod(AppearanceDetail.Skin_Color, skinColorIndex);
    }

    //Hair Model
    public void HairModelUp()
    {
        if (hairIndex < hairModels.Length - 1)
            hairIndex++;
        else
            hairIndex = 0;

        ApplyMod(AppearanceDetail.Hair_Model, hairIndex);
    }
    public void HairModelDown()
    {
        if (hairIndex > 0)
            hairIndex--;
        else
            hairIndex = hairModels.Length - 1;

        ApplyMod(AppearanceDetail.Hair_Model, hairIndex);
    }

    //Hair Color
    public void HairColorUp()
    {
        if (hairColorIndex < hairMats.Length - 1)
            hairColorIndex++;
        else
            hairColorIndex = 0;

        ApplyMod(AppearanceDetail.Hair_Color, hairColorIndex);
    }
    public void HairColorDown()
    {
        if (hairColorIndex > 0)
            hairColorIndex--;
        else
            hairColorIndex = hairMats.Length - 1;

        ApplyMod(AppearanceDetail.Hair_Color, hairColorIndex);
    }

    //Skin Color
    public void SkinColorUp()
    {
        if (skinColorIndex < skinMats.Length - 1)
            skinColorIndex++;
        else
            skinColorIndex = 0;

        ApplyMod(AppearanceDetail.Skin_Color, skinColorIndex);
    }
    public void SkinColorDown()
    {
        if (skinColorIndex > 0)
            skinColorIndex--;
        else
            skinColorIndex = skinMats.Length - 1;

        ApplyMod(AppearanceDetail.Skin_Color, skinColorIndex);
    }

    void ApplyMod(AppearanceDetail detail, int id)
    {
        switch (detail)
        {
            case AppearanceDetail.Hair_Model:
                if (activeHair != null)
                    GameObject.Destroy(activeHair);

                activeHair = GameObject.Instantiate(hairModels[id]);
                activeHair.transform.SetParent(hairAnchor);
                activeHair.transform.ResetTransform();
                ApplyMod(AppearanceDetail.Hair_Color, hairColorIndex);
                break;
            case AppearanceDetail.Skin_Color:
                for (int i = 0; i < bodyParts.Length; i++)
                {
                    bodyParts[i].material = skinMats[id];
                }
                break;
            case AppearanceDetail.Hair_Color:
                activeHair.GetComponent<MeshRenderer>().material = hairMats[id];
                break;
        }
    }

    public void SaveAspect()
    {
        SaveSystem.SavePlayerAspect(this);
        SceneManager.LoadScene(1);
    }
    public void LoadAspect()
    {
        PlayerAspectData data = SaveSystem.LoadPlayerAspect();
        hairIndex = data.hairModel;
        hairColorIndex = data.hairColor;
        skinColorIndex = data.skinColor;
        ApplyMod(AppearanceDetail.Hair_Model, hairIndex);
        ApplyMod(AppearanceDetail.Hair_Color, hairColorIndex);
        ApplyMod(AppearanceDetail.Skin_Color, skinColorIndex);
    }
}