using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardShip : MonoBehaviour
{
    [SerializeField]
    private GameObject player;
    [SerializeField]
    private GameObject shipCanvas;
    [SerializeField]
    private ShipMovement shipMovement;
    [SerializeField]
    private GameObject boardButton;

    // Start is called before the first frame update
    void Start()
    {
        boardButton.SetActive(false);
        //shipCanvas = GameObject.Find("ShipCanvas");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "BoardingTrigger")
        {
            boardButton.SetActive(true);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "BoardingTrigger")
        {
            boardButton.SetActive(false);
        }
    }

    public void BoardShipAction()
    {
        shipCanvas.SetActive(true);
        shipMovement.enabled = true;
        player.SetActive(false);
    }
}