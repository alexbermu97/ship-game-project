using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField]
    private VariableJoystick moveJ;
    [SerializeField]
    private VariableJoystick turnJ;
    [SerializeField]
    private Rigidbody rb;
    [SerializeField]
    private CinemachineVirtualCamera cam;
    [SerializeField]
    private Transform camLookAtObj;

    [SerializeField]
    [Range(0, 200)]
    private float moveSpeed;
    [SerializeField]
    [Range(0, 200)]
    private float hRotationSpeed;
    [SerializeField]
    [Range(0, 200)]
    private float vRotationSpeed;
    [SerializeField]
    private float minY;
    [SerializeField]
    private float maxY;

    float rotationX;
    float rotationY;

    Vector3 moveDir;

    [SerializeField]
    private BoxCollider groundDetection;
    public bool onGround;
    public bool inSpace;
    [Range(0, 100)]
    public float gravity;
    [Range(0, 100)]
    public float jumpForce;
    private float verticalVelocity;

    void Start()
    {
        onGround = false;
    }

    void Update()
    {
        RotatePlayer();
        UpdateVerticalVelocity();
    }

    void FixedUpdate()
    {
        MovePlayer();
        //Fall();
    }

    void MovePlayer()
    {
        float h = moveJ.Horizontal;
        float v = moveJ.Vertical;

        moveDir = (h * transform.right + v * transform.forward);
        rb.velocity = moveDir * moveSpeed * Time.deltaTime;

        rb.velocity = new Vector3(rb.velocity.x, verticalVelocity * Time.deltaTime, rb.velocity.z);
    }
    void RotatePlayer()
    {
        rotationX += turnJ.Horizontal * hRotationSpeed * Time.deltaTime;
        if (rotationX >= 0.2 || rotationX <= -0.2)
            transform.localEulerAngles = new Vector3(0, rotationX, 0);

        rotationY += turnJ.Vertical * vRotationSpeed * Time.deltaTime;
        rotationY = Mathf.Clamp(rotationY, minY, maxY);
        if (rotationY >= 0.2 || rotationY <= -0.2)
            camLookAtObj.localEulerAngles = new Vector3(-rotationY, 0, 0);

        /*if (rotationX >= 0.2 || rotationX <= -0.2)
            transform.Rotate(0, rotationX * hRotationSpeed * Time.deltaTime, 0);
        if (v >= 0.2 || v <= -0.2)
            camLookAtObj.Rotate(v * vRotationSpeed * Time.deltaTime, 0, 0);*/
    }

    public void Jump()
    {
        verticalVelocity = jumpForce;
        //rb.AddForce(Vector3.up * jumpForce * 10, ForceMode.Impulse);
        onGround = false;
    }

    void UpdateVerticalVelocity()
    {
        if (onGround)
            verticalVelocity = -gravity * Time.deltaTime;
        else
            verticalVelocity -= gravity * Time.deltaTime;
    }

    void Fall()
    {
        if (!onGround && !inSpace)
        {
            rb.AddForce(Vector3.down * gravity * 10);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Ground")
            onGround = true;
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Ground")
            onGround = false;
    }
}